#include <stdio.h>
#include <stdlib.h>  
#include <assert.h>  


int* createArray(int size, int value);
void readArray(FILE *fp, int *newSize, int **newArray);
void printArray(int *array, int size);
int* my_realloc(int* ptr, int oldSize, int newSize);


int main(int argc, char* argv[]) {

  //Collect int values from command line to represent size and initialization value
  if (argc < 3) {
    printf("Too few command-line arguments.\nUsage: %s <size> <value>\n", argv[0]);
    return 1;
  }
  int size = atoi(argv[1]);
  int value = atoi(argv[2]);

  if (size < 1) {
    printf("Size argument %d too small.  Please run with size > 0.\n", size);
    return 1;
  }
  
  printf("Creating an array with %d elements all initialized to %d.\n\n", size, value);
  

  int* list = createArray(size, value);
  //TODO - Part 2: Add code here that calls the createArray function 
  //to assign to "list" an array with "size" int elements 
  //initalized to "value"

  

      
  //Output all of the values in the array that was created by the function
  //TODO - Part 4: factor this code out into a function
  int* cur = list;
  if (cur) {
    for (int i = 0; i < size; i++) {
      printf("%d ", *cur);
      cur++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }
  /* Uncomment when ready to test Part4: */
  //printArray(list, size);

  // TODO - Part 3: fill in readArray function so this works correctly
  int list2Size = 0;
  int *list2 = NULL;
  // note that we use stdin as our filehandle, which means the function
  // will read input interactively; the function, however, shouldn't
  // care what filehandle it gets
  readArray(stdin, &list2Size, &list2);
  
  // TODO - Part 4: replace with function call when part 4 function works
  cur = list;
  if (cur) { // only print if cur isn't NULL (remember, NULL = 0 = false)
    for (int i = 0; i < size; i++) {
      printf("%d ", *cur);
      cur++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }

  //TODO - Part 5: make this work properly
  //Call my_realloc function to double array size
  list = my_realloc(list, size, size*2);
  size *= 2;

  //Output all of the values in the array that was newly reallocated
  // TODO - Part 4: replace with function call when part 4 function works
  cur = list;
  if (cur) { // only print if cur isn't NULL (remember, NULL = 0 = false)
    for (int i = 0; i < size; i++) {
      printf("%d ", *cur);
      cur++;
    }
    printf("\n\n");
  } else {
    printf("[ null ]\n");
  }

  //TODO - all parts: Perform any final "clean-up" needed by this program

  free(list);


  
  
  return 0;
}



/*
  Function that returns an array of "size" int elements,
  where each position has been initalized to "value"
*/
int* createArray(int size, int value){

  
  int *p;
  p = malloc(size * sizeof(size));
  int *temp = *p;
  for(int i = 0; i < size; i++){
    *(p+i) = value;
  }
  
  return p;

}


/*
  Function that returns a newly allocated array with values
  obtained from the given filehandle.  The first number
  read will be treated as the size of the new array, and
  the function will then allocate the array and attempt to
  fill it with the requested number of integers. 
*/
void readArray (FILE *fp, int *newSize, int **newArray) {

//TODO - Part 3: function stub, doesn't do anything, but makes sure
//  it reports that it hasn't done anything.
//  Fill in actual code.
//  Be sure to read from the filehandle using fscanf(); 
//  DO NOT just read from stdin using scanf().
  *newSize = 0;
  *newArray = NULL;
}


void printArray(int *array, int size) {

  //TODO - Part 4: Fill in function defintion here

}


int* my_realloc(int* ptr, int oldSize, int newSize) {
  
  //TODO - Part 5: Function stub, replace with real function.
  //  Doesn't do anything, returns a NULL pointer to indicate
  //  it didn't actually allocate more space
  return NULL;

}
