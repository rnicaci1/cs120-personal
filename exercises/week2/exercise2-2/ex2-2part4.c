#include <stdio.h>

int main(){
  int ar[5] = {0,0,0,0,0};
  int ar2[] = {};
  int sum = 0;
  int avg = 0;

  printf("Please enter five integers (spaces in between): ");
  for(int i = 0; i < 5; i++){
    int x;
    scanf("%d", &x);
    ar[i] = x;
    sum += x;
  }

  avg = sum/5;
  printf("You inputted: {%d, %d, %d, %d, %d}\n", ar[0], ar[1], ar[2], ar[3], ar[4]);
  printf("Average is: %d\n", avg);
  
  printf("Numbers greater than the average are: ");
  for (int j =0; j < 5; j++){
    if(ar[j] > avg){
      ar2[j] = ar[j];
      printf("%d ", ar[j]);
    }
 
  }

  printf("Array 2: {%d, %d}", ar2[0], ar[2]);
  printf("\n");
  return 0;
}
