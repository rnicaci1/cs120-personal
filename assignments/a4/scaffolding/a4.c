/**************************************
 * a4.c 
 *
 * <Rebecca Nicacio>
 * <Last Modified: 10/3/16>
 *
 * This program is designed to search for words
 * the user enters from a grid in an input file 
 *
 *************************************/

#include <stdio.h>
#include <string.h>
#include "wordsearch.h"

//This is the main function of the program, in charge of calling all other functions and actions
int main (int argc, char* argv[]) {                
    char inFile[20];                            
    strcpy(inFile, argv[1]);                     //accepts a command line argument as the file name     
    char grid[MAX_GRID_SIZE][MAX_GRID_SIZE];     //initializes a 2D array the max size allowed
    
    int square = checkGrid(inFile);              //if the grid is valid, this will store its dimensions  
    if(square != -1){
      readInput(inFile, grid);                   //executes the functions if the grid is valid
      findWord(grid, square);                     
    } else {
      printf("Grid Invalid!");
    }
    
  return 0;
}
