/* wordsearch.c
 *
 * <Rebecca Nicacio>
 * <Last Modified: 10/3/16>
 *
 * Functions for checking the validity of the grid, reading file and user input
 * and for finding inputted words in the grids. So far, these functions can do three of these
 * things, however, they cannot find the exact word in the grid. The function findWord() can
 * locate the letters of the inputted word in the grid.
 * 
 */

#include <stdio.h>
#include <string.h>
#include "wordsearch.h"

//check to see if the grid is valid by comparing the rows and columns
int checkGrid(char inFile[]){
  FILE* in = fopen(inFile, "r");    //opens the file to be read  
  char c;
  int rows = 0, cols = 0, temp = 0; //these are the stored numbers of rows and columns, rows and cols will be incremented as the file is read
  while((c = fgetc(in)) != EOF){
    temp++;                         //the stored number of columns will be used to compare with the rest of the grid
    if(c == '\n'){
      if(rows != 0){
	if(temp != cols){           //returns false if the subsequent columns don't match up with the stored value
	  return -1;
	}
      }
      cols = temp;                  //stores the number of columns from the previous row
      temp = 0;                     //resets the column comparison value for the beginning of a new row
      rows++;                       //the next row
    }
  }
  rows++;                           //rows begin at 0, so it must be incremented if it is to equal the number of columns
  fclose(in);                       //closes the file
  if(rows!=cols || rows >= 10 || cols >= 10){
    return -1;                      //returns false if the rows or columns are greater that the max limit, or if the number of rows
  } else {                          //and columns aren't equal   
    return cols;                    
  }
  return cols;                      //otherwise, the function returns the dimensions of the grid 
}

//read the grid within the inputted file, storing its values in a 2D array
void readInput(char inFile[], char grid[MAX_GRID_SIZE][MAX_GRID_SIZE]){
  FILE* in = fopen(inFile, "r");    //opens the file
  int row = 0;                      
  int col = 0;
  char c;

  
  while((c = fgetc(in)) != EOF){    //for the duration of the file, store the values in the array
    grid[row][col] = c;             //stores the value of the character at c into the array
    col++;                          //increments the number of columns  
    if(c == '\n'){                  //increments the number of rows if a newline character is read 
      row++;
      col = 0;                      //resets the value of the columns for the beginning of the next row.
    }   
  }
  
  fclose(in);                       //closes the file  
}

//Attempts to locate a user inputted word in the stored grid.
//As of right now, this function can only find the occurences of the letters in the word, not the word itself
void findWord(char grid[MAX_GRID_SIZE][MAX_GRID_SIZE], int square){
  int i = 0;
  char word[10];
  
  while(!isspace(word[i] = getchar())){ 
    i++;                            //stores the value of the inputted word in an array for comparison
  }
  
  int horiz = 0;
  int vert = 0;
 
  for(int j = 0; j < square; j++){  //This locates the letters of the words and keeps track of their occurrences
    for(int k = 0; k < square; k++){
      if(word[k] == grid[j][k] && word[k] != grid[j+1][k]){
	printf("%d, %c", k, word[k]);
	horiz = j;                  //this is intended to keep track of whether the letters are found along a row or a column
      }                             //however, it fails to differentiate between the two 
      if(word[j] == grid[j][k] && word[j] != grid[j][k+1]){
	printf("%d, %c", j, word[j]);
	vert = k;
      }
    }
  }
  if(horiz > 0){                    //since both the values of horiz and vert will be greater that 0, it can't differentiate
    printf("found it horizontal %d %d", horiz, vert);
  } else if (vert > 0){             //with more development, this section should be able to locate the word in its entirety.
    printf("found it vertical");
  }
}

