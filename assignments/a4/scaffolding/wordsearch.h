x/* wordsearch.h
 *
 * <Rebecca Nicacio>
 * <Last Modified: 10/3/16>
 *
 * Headers for functions needed for a4.c
 * Prototypes of the functions found in wordsearch.c
 * 
 */

#ifndef _CS120_SCAFFOLD_WORDSEARCH_H
#define _CS120_SCAFFOLD_WORDSEARCH_H

#include <stdio.h> // you'll need this if you want file handles as arguments 

#define MAX_GRID_SIZE 10 // constant for max grid size
#define WORD_BUFFER_LENGTH 11 // space for 10 "word" chars plus a null-termination

int checkGrid(char inFile[]);

void readInput(char inFile[], char grid[MAX_GRID_SIZE][MAX_GRID_SIZE]);

void readWord(char grid[MAX_GRID_SIZE][MAX_GRID_SIZE], int square);
#endif
