/* a7aHelper.cpp
 * Rebecca Nicacio
 * Date Last Modified: 11/7/16
 *
 * These are the function definitions for Part A
 * It includes the functions to order alphabetically,
 * reverse alphabetically, and by frequency
 */

#include "a7aHelper.hpp"

/*read the inputted file and store the values in a map*/
map<string,int> read(){
  map<string, int> m1;      //Create the map
  string str1, str2, str3;
  str1 = "<START>";

  while(cin >> str2){       //Go for as long as there is a next value in the fiel
    str3 = str1 + " " + str2; //This string will be stored in the map as the bigram
    m1[str3]++;             //Count the frequency of the bigram and store it
    str1 = str2;            //reset the first bigram string in order to move on
  }

  str3 = str1 + " " + "<END>";
  m1[str3]++;               //Account for the ending bigram
  return m1;
}


/*Takes in the created map in prints it (to the console or file)*/
int aOrder(map<string,int> m1){
  for(map<string, int>::iterator iter = m1.begin(); iter != m1.end(); iter++){
    cout << iter->first << " " << iter->second << "\n"; //Go through every element in 
  }                                                     //the map and print both its
  return 0;                                             //values
}

/*Takes in the created map and outputs it in reverse alphabetical order*/
int rOrder(map<string,int> m1){
  for(auto iter = m1.rbegin(); iter != m1.rend(); iter++){ //Start from the end and
    cout << iter->first << " " << iter->second << "\n";    //go to the beginning
  }                                                        //output each element
  return 0;
}

/*Takes in the created map and outputs it in order by frequency*/
int cOrder(map<string,int> m1){
  vector<pair<string, int> > v1(m1.begin(), m1.end());   //Store each value of the map (the integer of each element) into a vector
  sort(v1.begin(), v1.end(), countComp);                 //Sort the elements in this vector by using countComp as a comparison

  for(auto iter = v1.begin(); iter != v1.end(); iter++){ //Output the map using the vector's order
    cout << iter->first << " " << iter->second << "\n";
  }

  return 0;
}

/*Takes in two integer values from the vector and compares them */
bool countComp(const pair<string,int> &i, const pair<string, int> &j){
  if(i.second < j.second){         //return if the first integer is smaller than the second 
    return 1;                      //this will mean that the element with the smaller value will come first
  } else if(i.second == j.second){ //In the case the values are equal, order by alphabetical
    if (i.first < j.first){
      return 1;
    } else {
      return 0;
    }
  } else {                          //Move on otherwise
    return 0;
  }
}

