/* a7bHelper.cpp
 * Rebecca Nicacio
 * Date Last Modified: 11/7/16
 * 
 * These are the function definitions for Part B,
 * they include a functin to create a map out of a file of bigrams
 * and a function to creat randomly generated texts out of those bigrams
 */

#include "a7bHelper.hpp"

/*Reads through the file and creates a map out of the bigrams*/
map<string, vector<string> >  makeMap(){  //assumes, if not an empty file, acorrectly formatted one
  map<string, vector<string> > m1;
  string str1, str2;
  int val;
  while(cin >> str1 && cin >> str2 && cin >> val){
    for(int i = 0; i < val; i++){ //for every instance of a first word in a bigram
      m1[str1].push_back(str2);   //find that other words that follow it and assign them as the element value 
    } //(ie, "he said" and "he ate" both start with "he", so "said" and "ate" are assigned to "he")
  }


  if(m1.size() == 0){  //Checks for an invalid file (ie an empty one)
    std::cerr << "Unable to read!";
    exit(1);
  }
  return m1;
}

/*Takes the created map and outputs randomly generated texts out of the elements*/
int output(map<string, vector<string> > m1){
  string str = "<START>";  //begin the text with start
  cout << str << " ";
  while(str.compare("<END>") != 0){ //Go until the function hits the bigram with <END>
    int i = (int)(rand() % m1[str].size()); //Creates a random number
    str = m1[str][i];                       //Uses that number to choose another string out of the vector assigned to the word
    cout << str << " ";
  }

  cout << "\n";
  return 0;
}
