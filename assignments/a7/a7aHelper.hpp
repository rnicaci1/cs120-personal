/*a7aHelper.hpp
 *These are the function prototypes for Part A
 */
#ifndef _A7AHELPER_HPP
#define _A7AHELPER_HPP

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <utility>

using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::sort;
using std::pair;

map<string,int> read();
int aOrder(map<string,int> m1);
int rOrder(map<string,int> m1);
int cOrder(map<string,int> m1);
bool countComp(const pair<string,int> &i, const pair<string,int> &j);

#endif
