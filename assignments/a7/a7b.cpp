/* a7b.cpp
 * Intermediate Programming
 * Section 2 Assignment 7
 * Rebecca Nicacio
 * Date Last Modified: 11/7/16
 * 
 * This program holds the main funtion for Part B.
 * It takes in the command line arguments and the input file.
 * It also calls other Part B functions, such as creating the randomly
 * generated texts.
 */

#include "a7bHelper.hpp"

int main(int argc, char* argv[]){
  if(argc != 2){                    //Error message for too many or too few  
    std::cerr << "Invalid Input!";  //command line arguments
    exit(1);
  }
  
  map<string, vector<string> > m1 = makeMap(); //Create a map out of the inputted file

  int i = atoi(argv[1]);            //Convert the command line arg to an integer
  for(int go = 0; go < i; go++){
    output(m1);                     //Print the randomly generated texts (to the console or
  }                                 //to a file) 
  return 0;
}
