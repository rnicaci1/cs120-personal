/* a7a.cpp
 * Intermediate Programming
 * Section 2 Assignment 7
 * Rebecca Nicacio
 * Date Last Modified: 11/7/16
 * 
 * This program is the main function for Part A
 * It takes in the input file and command line arguments
 * and calls the other pertinent functions 
 */

#include "a7aHelper.hpp"

int main(int argc, char* argv[]){
  if (argc != 2) {                //Error message for too few or too many command
    std::cerr << "Invalid Input!\n"; //line arguments
        return 1;
  }
  
  map<string,int> m1 = read();    //inserts the input file into a map

  if(m1.size() <= 1){             //Error message for an invalid file
    std::cerr << "Unable to read! Please try again!\n"; 
    exit(1);                      //If this is the case, then the program ends 
    
  } else {
  
    if(string(argv[1]) == "a"){   //User enters 'a', bigrams are sorted alphabetically
      aOrder(m1);
    } else if (string(argv[1]) == "c"){  //User enters 'c', bigrams are sorted by frequency
      cOrder(m1);
    } else if (string(argv[1]) == "r"){  //User enters 'r', bigrams are sorted in reverse alphabetical
      rOrder(m1);
    } else {                      //Error message for invalid inputs      
      std::cerr << "Invalid Input! Please use : \"a\", \"c\", or \"r\"\n";
      exit(1);
    }
    
  }
      return 0;
}

