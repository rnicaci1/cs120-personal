/*a7bHelper.hpp
 *These are the function prototypes for Part B
 */
#ifndef _A7BHELPER_HPP
#define _A7BHELPER_HPP

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <utility>
#include <cstdlib>

using std::cin;
using std::cout;
using std::vector;
using std::string;
using std::map;
using std::sort;
using std::pair;

map<string, vector<string> > makeMap();
int output(map<string, vector<string> > m1);
#endif
