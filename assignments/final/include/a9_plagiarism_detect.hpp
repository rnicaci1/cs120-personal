/* a9_plagiarism_detect.hpp
 * Intermediate Programming
 * Coco Li & Rebecca Nicacio
 * Date Last Modified: 12/5/16
 *
 * This file contains the Compare class and its method prototypes
 */

#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include "a9_process_document.hpp"
using std::map;
using std::string;
using std::pair;
using std::pow;
using std::sqrt;
using std::acos;
using std::cout;

/*Compare class that is used as an object to contain 2 documents for comparison*/
class Compare{
public:
  Compare() {;}
  void calcTermFrequency(Document *doc); /*Calculates the term frequency for a document*/

  double compare(); /*compare function, uses the other functions to 
		      calculate the similarity between two documents*/

  double calcSim(Document *doc1, Document *doc2); /*Calculates the numerical value for the
						    similarity between two documents*/

  void calcIDF(); /*Calculates the inverse document frequency*/
  
  Document source; //the first document for comparison
  Document other;  //the second document for comparison
  
};
