/* a9_list_control.hpp
 * Intermediate Programming
 * Coco Li & Rebecca Nicacio
 * Date Last Modified: 12/6/16
 *
 * This file contains the List class and its methods, which read the
 * overall document set and stores the names into a vector  
 */
#include <iostream>
#include <vector>
#include <fstream>

/*List class, which reads and controls the overall doc set*/
class List{
 public:
  void eraseLast(){list.pop_back();}                   //erases the last element of the list   
  std::vector<std::string> returnList(){return list;}  //returns the list of documents as names stored in a vector
  int returnNumFiles(){return numFiles;}               //returns the overall number of documents found in the set 
  int size(){return list.size();}                      //returns the size of the list

  /*reads the file names from the doc set and stores them in a vector for access later*/
  void readFiles(std::string listFile){
    std::ifstream fin;
    fin.open(listFile);
    if(!fin.is_open()){
      std::cerr << "Unable to open '" << listFile << "'\n";
      exit(1);
    }
    
    std::string name;
    while(fin >> name){
      list.push_back(name);                    //Add the names to the vector
      numFiles++;                              //Increment the number of files
    }
    fin.close();
  }
    
 private:
  std::vector<std::string> list;               //the vector of document names
  int numFiles = 0;                            //the number of docs in the set
};
