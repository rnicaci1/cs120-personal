#ifndef _A9_PROCESS_DOCUMENT_HPP
#define _A9_PROCESS_DOCUMENT_HPP

/* Intermediate Programming
 * Coco Li & Rebecca Nicacio
 * Date Last Modified: 12/6/16
 *
 * This file contains the Document class and its method prototypes, 
 * which control reading in a document and breaking it down into n-grams
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <string>
#include <utility>

using std::map;
using std::string;
using std::vector;
using std::to_string;

/*Document class, which controls reading in individual documents*/
class Document {
public:
  Document(){}                                                                 //Constructor

  void readFileList(std::vector<std::string> listFile, int index, unsigned n); //Read individual documents from the doc list
  void makeModel(std::string fileName, unsigned n);                            //Break down the document into n-grams
  std::map< std::vector<std::string>, int> returntxt(){return model;}          //Returns the n-grams for that document and their frequencies
  
  std::map<std::vector<std::string>, std::pair<double, double> > weighted;     //Creates a map for the weighted values for each of the n-grams
  std::map<std::vector<std::string>, int> model;                               //Creates the map for the n-grams and their frequencies
  void clear();                                                                //Clear the Document to continuously reuse the class 
  
};

#endif  
