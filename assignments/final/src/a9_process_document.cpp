/* a9_process_document.cpp
 * Intermediate Programming
 * Coco Li & Rebecca Nicacio
 * Date Last Modified: 12/5/2016
 *
 * This file contains the methods for reading in and processing
 * the documents into n-grams for comparison. SEE README
 */

#include "../include/a9_process_document.hpp"

/*Read the individual files from the list*/
void Document :: readFileList(vector<string> listFile, int index, unsigned n){
  makeModel(listFile[index], n);
}

/*Creates the n-grams out of the inputted files*/
void Document :: makeModel(string fileName, unsigned n){
  /*if file doesn't open correctly, throw error*/
  std::ifstream infile;
  infile.open(fileName);
  if (!infile.is_open()) {
    std::cerr << "File name provided does not exist!\n";
    exit(1);
  }

  vector<string > key;
  string word1;
  /*Add the starting n-gram*/
  for(unsigned i = 1; i < n; ++i){
    string temp = "<START_" + to_string(i) + ">";
    key.push_back(temp);
  }

  /*Checks for the first word, deals with empty file and one word file*/
  if(infile >> word1){
    key.push_back(word1);
  }else{
    std::cerr << "No input, try again. \n";
    exit(1);
  }
  
  model[key] = 1;
  
  /*keep incrementing map until reach the last word*/
  while(infile >> word1){
    if((word1 == "the") || (word1 == "to") || (word1 == "of") || (word1 == "and") ||
       (word1 == "a") || (word1 == "in") || (word1 == "that") || (word1 == "it") ||
       (word1 == "as") ||(word1 == "are") || (word1 == "at") || (word1 == "by") ||
       (word1 == "on") || (word1 == "with") || (word1 == "for") || (word1 == "from")){
      infile >> word1;          //Skip low-weight words (words that are very common) for effeciency
    }                           //Words obtained from Wikipedia

    /*ignores punctuation and upper/lower case to decrease Ngrams*/
    string temp;
    for(auto it = word1.begin(); it != word1.end(); ++it){
      if(!ispunct(*it)){
	temp.push_back(tolower(*it));
      }
    }
    word1 = temp;
    
    key.erase(key.begin());
    key.push_back(word1);       //Creates n-grams for the rest of the document 

    if(model.count(key)){       //Stores the n-grams, if the term already exists, 
      ++(model.at(key));        //increment its frequency
    }else {                     //If the n-gram didn't exist before, make a new key for it
      model[key] = 1;
    }
  }

  /*deal with the different numbers of endings after the last word was read in*/
  for(unsigned i = 1; i < n; ++i){ //Adds the ending n-gram
    key.erase(key.begin());
    string temp = "<END_" + to_string(i) + ">"; 
    key.push_back(temp);
    model[key] = 1;
    word1 = temp;
  }

  infile.close();
}

/*Empties the data in the document*/
void Document :: clear(){  
  model.clear();
  weighted.clear();
}
