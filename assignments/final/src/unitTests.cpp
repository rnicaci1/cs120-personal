#include "../include/catch.hpp"
#include "../include/a9_process_document.hpp"
#include "../include/a9_list_control.hpp"
#include "a9_plagiarism_detect.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>

using std::string;
using std::vector;
using std::map; 

/*unit tests for a9_process document*/
/*test the makeModel() function in Document class*/
TEST_CASE("makeModel", "[makeModel]"){
  Document doc1;
  doc1.makeModel("../data/test1.txt", 2);//makes bigram of the text in test1.txt
  string result1;
  map<vector<string>, int> text1 = doc1.returntxt();

  for(auto iter = text1.begin();//to_string the bigram
      iter != text1.end();
      iter++){
    for(auto iter2 = iter->first.begin();
	iter2 != iter->first.end();
	++iter2){
      result1 += *iter2 + " "; 
    }
    result1 += to_string(iter->second) + "\n"; 
  }

  
  Document doc2;
  doc2.makeModel("../data/test2.txt", 3);//makes trigram of the text in test2.txt
  string result2 = "";
  map<vector<string>, int> text2 = doc2.returntxt();

  for(auto iter = text2.begin();//to_string the bigram
      iter != text2.end();
      iter++){
    for(auto iter2 = iter->first.begin();
	iter2 != iter->first.end();
	++iter2){
      result2 += *iter2 + " ";
    }
    result2 += to_string(iter->second) + "\n";
  }

  Document doc3;
  doc3.makeModel("../data/test1.txt", 1);//makes unigram model of the text in test1.txt
  string result3;
  map<vector<string>, int> text3 = doc3.returntxt();

  for(auto iter = text3.begin();//to_string the unigram model
      iter != text3.end();
      iter++){
    for(auto iter2 = iter->first.begin();
	iter2 != iter->first.end();
	++iter2){
      result3 += *iter2 + " ";
    }
    result3 += to_string(iter->second) + "\n";
  }
  

  REQUIRE(result1 == "<START_1> note 1\nSome us 1\nare serious, 1\ncomedy. <END_1> 1\ndifferences the 1\nfamily. Some 1\nhuman family. 1\nnote obvious 1\nobvious differences 1\non comedy. 1\nserious, some 1\nsome thrive 1\nthe human 1\nthrive on 1\nus are 1\n");
  REQUIRE(result2 == "<START_1> <START_2> note 1\n<START_2> note obvious 1\nSome us are 1\na human family. 1\nare serious, some 1\ncomedy. some us 1\ndifferences a human 1\nfamily. Some us 1\nhuman family. Some 1\nmany differences a 1\nnote obvious many 1\nobvious many differences 1\non comedy. some 1\nserious, some thrive 1\nsome thrive on 1\nsome us thrive. 1\nthrive on comedy. 1\nthrive. <END_1> <END_2> 1\nus are serious, 1\nus thrive. <END_1> 1\n");
  REQUIRE(result3 == "Some 1\nare 1\ncomedy. 1\ndifferences 1\nfamily. 1\nhuman 1\nnote 1\nobvious 1\non 1\nserious, 1\nsome 1\nthe 1\nthrive 1\nus 1\n");
  
}

/*tests  N-gram models from the files that we read in*/
TEST_CASE("readFileList", "readFileList"){
  List list;
  list.readFiles("../data/testList.txt");
  
  Document doc1;
  doc1.readFileList(list.returnList(), 0, 2);//makes bigram of the first file in testList.txt
  string result1;
  map<vector<string>, int> text1 = doc1.returntxt();

  for(auto iter = text1.begin();//to_string the bigram model stored in doc1
      iter != text1.end();
      iter++){
    for(auto iter2 = iter->first.begin();
	iter2 != iter->first.end();
	++iter2){
      result1 += *iter2 + " ";
    }
    result1 += to_string(iter->second) + "\n";
  }

  Document doc2;
  doc2.readFileList(list.returnList(), 1, 3);//makes trigram of the second file in testList.txt  
  string result2;
  map<vector<string>, int> text2 = doc2.returntxt();

  for(auto iter = text2.begin();//trigram the trigram model
      iter != text2.end();
      iter++){
    for(auto iter2 = iter->first.begin();
	iter2 != iter->first.end();
	++iter2){
      result2 += *iter2 + " ";
    }
    result2 += to_string(iter->second) + "\n";
  }

  Document doc3;
  doc3.readFileList(list.returnList(), 0, 1);//makes unigram model of the first file in testList.txt
  string result3;
  map<vector<string>, int> text3 = doc3.returntxt();

  for(auto iter = text3.begin();//to_string unigram model
      iter != text3.end();
      iter++){
    for(auto iter2 = iter->first.begin();
	iter2 != iter->first.end();
	++iter2){
      result3 += *iter2 + " ";
    }
    result3 += to_string(iter->second) + "\n";
  }

  REQUIRE(result1 == "<START_1> note 1\nSome us 1\nare serious, 1\ncomedy. <END_1> 1\ndifferences the 1\nfamily. Some 1\nhuman family. 1\nnote obvious 1\nobvious differences 1\non comedy. 1\nserious, some 1\nsome thrive 1\nthe human 1\nthrive on 1\nus are 1\n");
  REQUIRE(result2 == "<START_1> <START_2> note 1\n<START_2> note obvious 1\nSome us are 1\na human family. 1\nare serious, some 1\ncomedy. some us 1\ndifferences a human 1\nfamily. Some us 1\nhuman family. Some 1\nmany differences a 1\nnote obvious many 1\nobvious many differences 1\non comedy. some 1\nserious, some thrive 1\nsome thrive on 1\nsome us thrive. 1\nthrive on comedy. 1\nthrive. <END_1> <END_2> 1\nus are serious, 1\nus thrive. <END_1> 1\n");
  REQUIRE(result3 == "Some 1\nare 1\ncomedy. 1\ndifferences 1\nfamily. 1\nhuman 1\nnote 1\nobvious 1\non 1\nserious, 1\nsome 1\nthe 1\nthrive 1\nus 1\n");
}


/*tests functions from a9_list_control*/
/*tests the readFile function in list, whether it successfully reads in lists*/
TEST_CASE("readFile", "[readFile]"){
  List list1; 
  list1.readFiles("../data/testList.txt");
  vector<string> listname1= list1.returnList();
  string names1;
  int numFiles1 = list1.returnNumFiles();

  int i = 1; 
  for(auto iter = listname1.begin();
      iter != listname1.end();
      ++iter){
    string name1 = "../data/test" + to_string(i) + ".txt"; 
    REQUIRE(*iter == name1);
    ++i;
  }

  List list2;
  list2.readFiles("../data/tempList.txt");
  vector<string> listname2= list2.returnList();
  string names2;
  int numFiles2= list2.returnNumFiles();

  for(auto iter = listname2.begin();
      iter != listname2.end();
      ++iter){
    names2 += *iter + "\n";
  }
  
  
  REQUIRE(numFiles1 == 6);

  REQUIRE(names2 == "../data/test4.txt\n../data/test3.txt\n");
  REQUIRE(numFiles2 == 2);
}

/*test the eraseLast() function in the a9_list_control*/
TEST_CASE("eraseLast", "[eraseLast]"){
  List list1;
  list1.readFiles("../data/testList.txt");
  for(int i = list1.returnNumFiles() -1; i>= 0 ; --i){
    string name = "../data/test" + to_string(i+1) + ".txt";
    string cur = (list1.returnList())[i];
    REQUIRE(cur.compare(name) == 0);
    REQUIRE(list1.size() == i+1);
    REQUIRE(list1.returnNumFiles() == 6); 
    list1.eraseLast();
  }
}

/*test whether the numFile variable
 *in a9_list_control gets incremented correctly */
TEST_CASE("numFile", "[numFile]"){
  List list1;
  list1.readFiles("../data/testList.txt");
  REQUIRE(list1.returnNumFiles() == 6);
  list1.readFiles("../data/tempList.txt");
  REQUIRE(list1.returnNumFiles() == 8);
}


/*tests wether the size() function 
 *returns the correcct size of the 
 *vector containing the names in List class*/
TEST_CASE("size", "[size]"){
  List list1;
  list1.readFiles("../data/testList.txt");
  REQUIRE(list1.size() == 6);
  list1.readFiles("../data/tempList.txt");
  REQUIRE(list1.size() == 8); 
}


/*tests the compare() method from a9_plagiarism_detect
 *we didn't include tests for other because it was hard
 *to hand compute and eye view what the correct outputs should be*/
TEST_CASE("compare", "compare"){
  Compare comp1;
  comp1.source.makeModel("../data/test1.txt", 2);
  comp1.other.makeModel("../data/test2.txt", 2);

  double sim1 = comp1.compare();
  string ifsim1 = "false";
  if(sim1 > 0.68){
    ifsim1 = "true";
  }


  Compare comp2;
  comp2.source.makeModel("../data/test2.txt", 3);
  comp2.other.makeModel("../data/test3.txt", 3);

  double sim2 = comp2.compare();
  string ifsim2 = "false";
  if(sim2 > 0.68){
    ifsim2 = "true";
  } 


  Compare comp3;
  comp3.source.makeModel("../data/test6.txt", 5);
  comp3.other.makeModel("../data/test3.txt", 5);

  double sim3 = comp3.compare();
  string ifsim3 = "false";
  if(sim3 > 0.68){
    ifsim3 = "true";
  } 
  
  REQUIRE(ifsim1 == "true");
  REQUIRE(ifsim2 == "false");
  REQUIRE(ifsim3 == "true");
}
