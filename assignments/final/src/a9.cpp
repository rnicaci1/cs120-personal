/* a9.cpp
 * Intermediate Programming Fall 2016 Section 02
 * Coco Li (xli152) & Rebecca Nicacio
 * Date Last Modified: 12/5/16
 * This is the main file...
 */

#include <iostream>
#include <string>
#include <map>
#include "../include/a9_plagiarism_detect.hpp"
#include "../include/a9_list_control.hpp"

using std::cout;
using std::string;
using std::map;

int main(int argc, char* argv[]){
  if(argc < 2 || argc > 3){          //Check for correct number of command line arguments 
    std::cerr << "Incorrct number of command line arguments supplied. \n";
    return 1;
  }
  
  string files = argv[1];
  unsigned senseLevel = 3;           //Level of detection sensetivity, default to medium 

  if(argc == 3){                     //Check user input for detection sensetivity
    string sensitivity = argv[2];
    if(sensitivity == "l"){
      senseLevel = 4;                //Sensitivity level is the size the n-grams we will create for the doc
    } else if(sensitivity == "m") {
      senseLevel = 3; 
    } else if(sensitivity == "h") {
      senseLevel = 2;
    } else {
      std::cerr << "Incorrect sensitivity command, try again!";
      return 1; 
    }
  }

  List listFile;
  listFile.readFiles(files);         //read in the files from the document list  

  if(listFile.size() == 0){         //check if the file we read in was empty
    throw std::underflow_error("No file being compared!");
  }

  std::cout<<"running..." << "\n";
  int times = listFile.size() - 1;   //times keeps track of the number of docs left after comparison
  while(times >= 1){                 //Will go until there is only 1 doc remaining, since there is nothing left to compare with
    Compare comp;
    comp.source.readFileList(listFile.returnList(), times, senseLevel); //Reads the file at the end of the list

    for(int j = times - 1; 0 <= j; --j){ //Start from the end of the list of files and work backwards
      
      comp.other.readFileList(listFile.returnList(), j, senseLevel); //As the for loop moves through, this will be the other files left in the list
      double sim = comp.compare();       //value representing how similar 2 documents are calculated by cosine similarity

      if(sim >= 0.68){                   //check the value against the threshold for suspicious docs, print out file names if similar
	cout << (listFile.returnList())[times] << " " << (listFile.returnList())[j] << "\n";
      }

      comp.other.clear();            //clear the comparison document in order to take in the next one
      comp.source.weighted.clear();  //clear the weighted comparison map from the previous comparison to get ready for the next one
    }
    
    listFile.eraseLast();            //Discard the file just compared to the rest of the list (source file) 
    times = listFile.size() - 1;     //Decrement times
  }

  return 0;  
}
