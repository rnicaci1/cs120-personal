/* a9_plagiarism_detect.cpp
 * Intermediate Programming
 * Coco Li & Rebecca Nicacio
 * Date Last Modified: 12/5/16
 *
 * This file contains the methods needed to detect the similarities
 * between two documents. Utilizes Cosine Similarity, Vector Space Model,
 * and Term Frequency-Inverse Document Frequency (TF-IDF). SEE README 
 */

#include "../include/a9_plagiarism_detect.hpp"

/*Overall compare function, uses the other functions to 
 *calculate the similarity between two documents*/
double Compare :: compare(){
  double sim = 0;              //similarity will be a number between 0.5 and 1.0 
  if(source.model.size() != 0 && other.model.size()!= 0){
    calcTermFrequency(&source); //Calculate the raw frequency (times an n-gram appears) for the first doc
    calcTermFrequency(&other);  //Calculate the raw frequency for the second doc
    calcIDF();                 //Calculate the inverse document frequency
    sim = calcSim(&source, &other); //Calculate the similarity value between the two
    
  } else {                     //Check for invalid/empty documents
    std::cerr << "empty documents!";
    exit(-1);
  }
  
  return sim; 
}

/*adjust term frequency (times a word/n-gram appears) for the source document and calculates the term frequency (TF)*/
void Compare :: calcTermFrequency(Document *doc){
  double maxfreq = 0.0; 
  /*find the biggest raw frequency of the document (how often the most common n-gram appears)*/
  for(auto iter = doc->model.begin();
      iter != doc->model.end();
      ++iter){
    if(iter->second > maxfreq){ 
      maxfreq = iter->second; 
    }
  }
  
  /*adjust this for the new frequency word map*/
  for(auto iter = doc->model.begin();
      iter != doc->model.end();
      ++iter){
    double TF = 0.5 + 0.5 * ((iter->second)/maxfreq);   //for every n-gram in the doc, use the term frequency formula
                                                        //to calculate the relative term weight in the doc

    pair<double, double> weightedAverage(TF, 0.5);      //create a pair for the term that will store the TF and IDF
    doc->weighted[iter->first] = weightedAverage;       //defalt the IDF to 0.5(term only appears in one document, 1/2)
  }  
}

/*Function to calculate the inverse document frequency (IDF)*/
void Compare :: calcIDF(){
  for(auto iter = this->source.weighted.begin();        //the formula for the idf is log(N/nt) where N is the number of total documents
      iter != this->source.weighted.end();              //and nt is the number of docs that have the term in it. However, since we have 
      ++iter){                                          //only 2 docs at a time, we check if the term is found in both, which would be  
    if(other.weighted.count(iter->first)){              //log(2/2) = 0. In order to prevent a division by 0 and since the ratio between 
      iter->second.second = 1.0;                        //the two is 1, we determine/default the value to be 1.0, and we no longer need 
      other.weighted.at(iter->first).second = 1.0;      //to use log
    }
  }
}

/*Calculate the numerical document similarity according to 
  the Cosine Similarity theory and the Vector Space Model*/
double Compare :: calcSim(Document *doc, Document *doc2){ 
  double algNum = 0;   //Numerator for the Vector Space Model algorithm
  double sim = 0;      //Similarity
  double wij = 0;      //Weight vector for the first doc in the comparison  
  double wiq = 0;      //Weight vector for the second doc

  /*for every n-gram term in the docs, calculate the weight*/
  for(auto iter = (doc->weighted).begin();
      iter != (doc->weighted).end();
      ++iter){          

    //calculate the weight of the first doc, squaring so to find the abs value
    wij += pow((iter->second).first * (iter->second).second, 2.0);
     
    if((*doc2).weighted.count(iter->first)){             //if the term appears in the second doc, calculate its tf and idf for the second doc
      double w2TF = doc2->weighted.at(iter->first).first;//because for formula we need the word's TF-IDF in both documents
      double w2IDF = doc2->weighted.at(iter->first).second;
    
      wiq += pow(w2TF * w2IDF, 2.0);                     //calculate the weight of the second doc, squaring so to find the absolute value
      algNum += (((iter->second).first * (iter->second).second) * (w2TF * w2IDF));
    }
  }
  
  if(algNum != 0){                                      //Prevent 0 division again for if no term is similar in the 2nd doc
    sim = algNum/(sqrt(wij) * sqrt(wiq));               //The vector of similarity according to the Vector Space model

    if(sim == 1){                                       //A result of 1 would mean both docs are exactly the same
      return sim;                                       //so since arccos(1) = 0, we return 1 immediately
    } else{
      double dist = acos(sim)/M_PI;                     //Calculate the cosine similarity between the two docs
      sim = (1 - dist);                                 //normalize this value into the [0.5, 1.0] range
      return sim;
    }
    
  }else {                                              //if there is no term similar in two documents, there is not likely plagriasm
    return 0.5;                                        //so we return 0.5 indicating that they are completely different 
  }  
}
