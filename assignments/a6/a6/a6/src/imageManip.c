/*                                                                            
 *imageManip.c                           
 *Intermediate Programming                                                    
 *Name: Rebecca Nicacio & Coco Li                                
 *Date Last Modified: 10/23/16                                                
 *               
 *Handles all of the image manipulation functions:
 *swap, invert, crop, grayscale, brightness, contrast, blur, and sharpen.     
 *extra implemented function for extra credit(if it fits): blur by region.
 */


#include <stdio.h>
#include "imageManip.h"
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265359

/* inver color of the image*/
int inverse(Image* img);

/* swap the color channel of the image*/
int swap(Image *img);

/* crop the image to given size if the size is not out of bound*/
int crop(Image* img, int x1, int x2, int y1, int y2);

/* change picture into grayscale*/
int grayscale(Image *img);

/* change brightness of image*/
int brightness(Image *img, int amt);

/* change contrast of image by given scale*/
int contrast(Image *img, double scale);

/* helper function to contrast*/
double cast(double rgb, double scale);

/* helper function to prevent pixel values from overflowing*/
double cap(double a);

/* blur picture with gaussian filter with given sigma*/
int blur(Image *img, double sigma);

/* helper function in blur to square numbers*/
double sq(double b);

/* sharpens the image with given parameters: sigma, amount */
int sharpen(Image* img, double sigma, double amt);

/* extra function: only blurs in the given parameter */
int blurByRegion(Image* img, double sigma, int x1, int y1, int x2, int y2);




/*Takes in the read image and inverts the pixel color values*/
int inverse(Image* img){

  //invert RGB values in each pixel of the image
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    img->data[i].r = 255- img->data[i].r; //invert red
    img->data[i].g = 255- img->data[i].g; //invert green
    img->data[i].b = 255- img->data[i].b; //invert blue
  }
  
  return 0;
}

/*Takes in the read image and swaps the pixel color values
 *i.e. red becomes blue, blue becomes green */
int swap(Image *img){

  //swap the RGB values for each pixel
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    int temp = img->data[i].r;
    img->data[i].r = img->data[i].g; //swap red value
    img->data[i].g = img->data[i].b; //swap green value
    img->data[i].b = temp;           //swap blue value
  }
  
  return 0;
}

/*Takes in the read image as well as the cropping dimensions
 *and then crops the image*/
int crop(Image* img, int x1, int y1, int x2, int y2){

  //If the user inputs invalid cropping dimensions, return an error
  if((x1<0) || (y1<0) || (x2>img->cols) ||(y2>img->rows)||(x1>x2)||(y1>y2)){
    printf("Bad input: out of bounds!");
    return 1;
  }
  
  int newr = y2-y1;//parameter of new number of rows
  int newc = x2-x1;//parameter of new number of columns

  Pixel * temp = malloc(sizeof(Pixel)*newr*newc);//allocate new pixel array to hold the cropped image pixels 
  int i = 0;
  for(int r = 0; r<img->rows; r++){//loop through the entire original array
    for(int c = 0; c<img->cols; c++){
      if((y1<= r) && (r<y2) && (x1<=c) && (c<x2)){
	/*if the pixel in the original pixel array is within the cropped parameter, 
	  add it to the new pixel array storing the cropped pixels*/
	temp[i++]= img->data[(r*(img->rows))+c];
      }
    }
   }

  img->cols = newr;//the original rows and columns are read in inverse, so to fix this pass
  img->rows = newc;//the new file with the parameters of rows and columns reversed as well

  free(img->data); //free the old array of pixels
  img->data = temp;//set the image pointint to now the cropped array of pixels 

  return 0;
}

/*Takes in the image and changes each pixel's color value to a shade of gray*/
int grayscale(Image *img){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    int intense = (int)(0.30*img->data[i].r + 0.59*img->data[i].g + 0.11*img->data[i].b); //computes the intensity of gray for the given pixel
    img->data[i].r = intense; //set each color channel to the gray value
    img->data[i].g = intense;
    img->data[i].b = intense;
  }
  
  return 0;
}

/*Takes in the image and changes the brightness according to the user inputted amount*/
int brightness(Image *img, int amt){
  for(int i = 0; i < (img->rows) * (img->cols); i++){    
    img->data[i].r = (int)(cap(img->data[i].r + amt)); //for each pixel, add the amount of intensity and cap it to the
    img->data[i].g = (int)(cap(img->data[i].g + amt)); //255 scale. This will brighten the pixel
    img->data[i].b = (int)(cap(img->data[i].b + amt));
  }
  
  return 0;
}

/*Takes in the image and increases its contrast by the user inputted scale*/
int contrast(Image *img, double scale){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    img->data[i].r = (int)(cast(img->data[i].r, scale));
    img->data[i].g = (int)(cast(img->data[i].g, scale));
    img->data[i].b = (int)(cast(img->data[i].b, scale));
  }  
  return 0;
}

/*Applies the contrast scale to the rgb value and casts it to a range from -0.5 to 0.5*/
double cast(double rgb, double scale){
  rgb = (rgb - (255 / 2)) / 255; //Apply the contrast scale 
  rgb *= scale;                  //to the rgb value
  
  if(rgb > 0.5){                 //Cast the changed value to a range of -0.5 to 0.5
    rgb = 0.5;
  }
  if (rgb < -0.5){
    rgb = -0.5;
  }
  
  rgb = (rgb - (255 / 2)) * 255; //Convert the value back to the 0-255 scale
  cap(rgb);                      //Cap it to the 0-255 range 
  
  return rgb;
}

/*Cap the inputted value to a scale from 0-255*/
double cap(double a){
  if(a > 255){
    a = 255.0;
  }
  if(a < 0){
    a = 0;
  }
  return a;
}

int blur(Image *img, double sigma){
  /* the sigma is 0, return original picture, and prevents function from crashing by dividing by 0*/
  if(sigma == 0){
    return 0;
  }

  /* get size of gaussian matrix */
  int rc = (int)(10*sigma);
  if(rc % 2 == 0){ //makes sure that rc is odd
    rc = rc+1;
  }

  /* creates empty gaussian array*/
  double gaussian[rc][rc];

  /*temperary rbg variables that holds the sum of rgb with the weight of gaussian array around the pixel*/
  double tempr = 0;
  double tempb = 0;
  double tempg = 0;

  /*average to normalize the weight of gaussian*/
  double average = 0;

  /* calculate the weight of gaussian matrix */
  for(int x = 0; x<rc; x++){
    for(int y = 0; y < rc; y++){
      int dx = rc/2 - x;
      int dy = rc/2 - y;

      /*for every element of the matrix, calculate its distribution and weight */
      gaussian[x][y] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));

      /*the keeps track of the average of the gaussian matrix*/
      average = average+gaussian[x][y];
      
    }
  }

  /*normalizes the matrix*/
  for(int i = 0; i < rc; ++i){
    for(int j = 0; j< rc; ++j){
      gaussian[i][j] /= average;
    }
  }

  
  /*loop through individual pixels and apply gaussian filter*/
  for(int r = 0; r < img->rows; r++){
    for(int c = 0; c < img->cols; c++){
      
      /*for each individual pixel, apply gaussian filter to things around it*/
      for(int m = 0; m < rc; m++){
	for(int n = 0; n < rc; n++){

	  /* normalizes the relative coordinate in the gaussian array to those in the actual pixel array*/
	  int i = (-rc/2)+m;
	  int j = (-rc/2)+n;
	  
	  /* if the gaussian filter is within the boundary of the picture*/
	  if((0 <=r+i) && (0 <= c+j) && (r+i < img->rows) && (c+j < img->cols) ){

	    /*temporary variables holds all the rbg values around the center pixel
	     *each rgb value adjusted with the gaussian filter
	     * the weird c+j and r+i is there to accomodate that we read rows and columns opposite the first week*/
	    tempr = tempr + (double)img->data[(c+j)*(img->rows) + r+i].r * gaussian[m][n];
	    tempg = tempg + (double)img->data[(c+j)*(img->rows) + r+i].g * gaussian[m][n];
	    tempb = tempb + (double)img->data[(c+j)*(img->rows) + r+i].b * gaussian[m][n];
	    
	  }
	  
	}
      }

      /* adjust the new rbg of each pixel to those after applied gaussian array*/
      img->data[(c*(img->rows)) + r].r = (int)cap(tempr/average);
      img->data[(c*(img->rows)) + r].g = (int)cap(tempg/average);
      img->data[(c*(img->rows)) + r].b = (int)cap(tempb/average);

      /*set temporary rbg variables to 0 so can loop through for next pixel*/
      tempr = 0;
      tempb = 0;
      tempg = 0;
    }
  }


  return 0;
}


/*helper function used in blur to generate gaussian filter*/
double sq(double b){
  b = (b)*(b);
  return b;
}


/* sharpens the image with given parameters: sigma, amount */
int sharpen(Image* img, double sigma, double amt){

  /*creates temporary image struct to hold the same picture/pixel array as the given picture*/
  Pixel* ptemp = malloc(sizeof(Pixel)*img->rows*img->cols);
  Image *tempi = malloc(sizeof(Image));
  tempi->data = ptemp;
  tempi->rows = img->rows;
  tempi->cols = img->cols;

  /*makes sure the temporary picture holds the same pixel array as given picture*/
  for(int i = 0; i < (img->rows)*(img->cols); i++){
    tempi->data[i] = img->data[i];
  }

  blur(tempi, sigma);

  /*loops through individual pixels in the array to make sure that each pixel's rbg
   *is calibrated with the differences between blurred picture and original picture
   *then the difference is sharpened by amount, and made sure to not overflow with cap function*/
  for(int i = 0; i<(img->rows)*(img->cols); i++){
    img->data[i].r = cap(img->data[i].r + amt*(img->data[i].r - tempi->data[i].r));
    img->data[i].g = cap(img->data[i].g + amt*(img->data[i].g - tempi->data[i].g));
    img->data[i].b = cap(img->data[i].b + amt*(img->data[i].b - tempi->data[i].b));
  }


  /*free the temporary image struct to hold the differences*/
  free(ptemp);
  free(tempi);
  
  return 0;
}


/* extra function: only blurs in the given parameter */
int blurByRegion(Image* img, double sigma, int x1, int y1, int x2, int y2){

  /*create temporary image and pixel array to hold the original image */
  Pixel* ptemp = malloc(sizeof(Pixel)*img->rows*img->cols);
  Image *tempi = malloc(sizeof(Image));

  tempi->data = ptemp;
  tempi->rows = img->rows;
  tempi->cols = img->cols;

  for(int i = 0; i < (img->rows)*(img->cols); i++){
    tempi->data[i] = img->data[i];
  }

  /*crops the image and blurs the area that user wants to blur*/
  crop(tempi, x1, y1, x2, y2);
  blur(tempi, sigma);

  
  int i = 0;
  for(int r = 0; r<img->rows; r++){//loop through the entire original array
    for(int c = 0; c<img->cols; c++){
      if((y1<= r) && (r<y2) && (x1<=c) && (c<x2)){
	/*if the pixel in the original pixel array is within the crop blur parameter, 
	 *exchange it with blurred pixel*/
	img->data[(r*(img->rows))+c] = tempi->data[i];
	i++;
      }
    }
  }

  /*free the temporary image struct*/
  free(ptemp);
  free(tempi);

  
  return 0;
}
