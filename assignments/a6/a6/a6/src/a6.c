/* Intermediate Programming
 * a6.c
 * Rebecca Nicacio & Coco Li
 * Last Modified: 10/17/16
 * This is the main function, it will call the menu
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "menuUtil.h"

/*main function, calls the menu display that will
 *handle all the image functions */
int main () {
  menu();
  return 0;
}
