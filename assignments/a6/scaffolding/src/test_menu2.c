/*
 *menuUtil.c
 *Intermediate Programming
 *Name: Rebecca Nicacio & Coco Li
 *Date Last Modified: 10/21/16
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#define PI 3.14159265359
//#include "ppmIO.h"
//#include "imageManip.h"
//#include "menuUtil.h"

typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Pixel;

typedef struct _image{
  Pixel* data;
  int rows;
  int cols;
  int colors;
} Image;

Image readImage(char inFile[]);
int writePPMImageFile(Image *im, char *filename); 
int inverse(Image* img);
int swap(Image *img);
int menu();
int crop(Image* img, int x1, int y1, int x2, int y2);
int grayscale(Image *img);
int blur(Image *img, double sigma);
double cap(double a);
double sq(double b);
int contrast(Image *img, double scale);
int sharpen(Image* img, double sigma, double amt);
double cast(double rgb, double scale);


int main(){
  menu();
  return 0;
}


int menu(){//Crop* userInC, Image* userImg, Read* userR, Write* userW){
  printf("Main menu:\n");
  printf("        r <filename> - read image from <filename>\n");
  printf("        w <filename> - write image to <filename>\n");
  printf("        c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n");
  printf("        i - invert intensities\n");
  printf("        s - swap color channels\n");
  printf("        g - convert to grayscale [not done yet]\n");
  printf("        bl <sigma> - Gaussian blur with the given radius (sigma) [not done yet]\n");
  printf("        sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma) [not done yet]\n");
  printf("        br <amt> - change brightness (up or down) by the given amount [not done yet]\n");
  printf("        cn <amt> - change contrast (up or down) by the given amount [not done yet]\n");
  printf("        q - quit\n");

  Image img;
  int counter = 0;

  char *bufferc = malloc(sizeof(char)*150);
  scanf("%s", bufferc);

  while(strcmp("q", bufferc) != 0){
    if(strcmp("r", bufferc) == 0){

      scanf("%s", bufferc);
      img = readImage(bufferc);
      printf("%s", bufferc);
      counter = 1;

    }else if(strcmp("w", bufferc)==0){

      scanf("%s", bufferc);
       printf("%s", bufferc);
       writePPMImageFile(&img, bufferc);
      
    }else if(strcmp("i", bufferc)==0){

      inverse(&img);
       printf("in inverse");
    } else if(strcmp("s", bufferc)==0){
      
      swap(&img);
      printf("in swap");
    } else if(strcmp("c", bufferc) ==0){
      int x1, x2, y1, y2;
      scanf("%d", &x1);
      scanf("%d", &y1);
      scanf("%d", &x2);
      scanf("%d", &y2);
      crop(&img, x1, y1, x2, y2);

      printf("in crop %d %d %d %d", x1, y1, x2, y2);

    }else if (strcmp("g", bufferc) ==0){
      grayscale(&img);
    }else if(strcmp("bl", bufferc) ==0){

      double sigma = 0;
      scanf("%lf", &sigma);
      printf("sigma is : %lf\n", sigma);
      blur(&img, sigma);
      
      //printf("blur not yet inplemented");
    }else if(strcmp("sh", bufferc) ==0){

      double sigma = 0;
      double amt = 0;
      scanf("%lf %lf", &sigma, &amt);
      sharpen(&img, sigma, amt);
      
      //printf("sharpen not yet inplemented");
    }else if(strcmp("br", bufferc) ==0){
      printf("brightness change not yet inplemented");
    }else if(strcmp("cn", bufferc) ==0){
      printf("contrast change not yet implemented");
    }else if(strcmp("q",bufferc) == 0){
      printf("goodbye!");
      free(bufferc);
      return 0;
    }else {
      printf("bad input, try again");
    }

    scanf("%s",bufferc);
  }

  /*if(strcmp("q",bufferc) == 0){
    printf("goodbye!");
    free(bufferc);
    return 0;
  }*/
  if(counter!= 0){
    free(img.data);
  }

  free(bufferc);
  return 0;

}

Image readImage(char inFile[]) {
  Image img;
  FILE* fp = fopen(inFile, "rb");
  if(!fp){
    printf("Cannot find image file!");
    exit(1);//return NULL;
  }
  
  char buffer[2];
  for (int i = 0; i < 2; i++){
    buffer[i] = fgetc(fp);
  }
  if(buffer[0] == 'P' && buffer[1] == '6'){
    char c;
    
    while(!isspace(c = fgetc(fp)));
    c=fgetc(fp);
    while(c == '#'){
      while((c=fgetc(fp)) != '\n');
    }
   
    
    ungetc(c, fp);
    
    fscanf(fp, "%d %d %d", &(img.rows), &(img.cols), &(img.colors));
    
    // allocate space for pixels
    Pixel *pix = NULL;
    pix = malloc(sizeof(Pixel) * img.cols * img.rows);
    
    img.data = pix;
    //while(isspace(c = fgetc(fp)));
    //ungetc(c, fp);
    fread(img.data, sizeof(Pixel), (img.rows)*(img.cols), fp);
   
  }  
  fclose(fp);

  return img;
}


/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename) {
  FILE *fp = fopen(filename, "wb");
  if (!fp) {
    fprintf(stderr, "Error:ppmIO - unable to open file \"%s\" for writing!\n", filename);
    return 0;
  }
  
  /* write tag */
  fprintf(fp, "P6\n");
  /* write dimensions */
  fprintf(fp, "%d %d\n%d   ", im->rows, im->cols, im->colors);

  int written = fwrite(im->data, sizeof(Pixel), (im->rows)*(im->cols), fp);
    
  if (written != (im->rows * im->cols)) {
    fprintf(stderr, "Error:ppmIO - failed to write data to file!\n");
    return 0;
  }
  fclose(fp);
  return written;
}

int inverse(Image* img){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    img->data[i].r = 255- img->data[i].r;
    img->data[i].g = 255- img->data[i].g;
    img->data[i].b = 255- img->data[i].b;
  }
  return 0;
}

int swap(Image *img){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    int temp = img->data[i].r;
    img->data[i].r = img->data[i].g;
    img->data[i].g = img->data[i].b;
    img->data[i].b = temp;
  }
  return 0;
}


int crop(Image* img, int x1, int y1, int x2, int y2){
  if((x1<0) || (y1<0) || (x2>img->cols) ||(y2>img->rows)||(x1>x2)||(y1>y2)){
    printf("bad input: cropping out of bound!");
    return 1;
  }
  int newr = y2-y1;
  int newc = x2-x1;
  printf("%d %d", newr, newc);

  Pixel * temp = malloc(sizeof(Pixel)*newr*newc);
  //Pixel * curr = temp;
  //Image * orig = img;
  int i = 0;
  for(int r = 0; r<img->rows; r++){
    for(int c = 0; c<img->cols; c++){
      if((y1<= r) && (r<y2) && (x1<=c) && (c<x2)){
	temp[i++]= img->data[(r*(img->rows))+c];
	//*curr = img->data[r*img->cols+c];
	//curr++;
	//i++;
      }
    }
   }
  /*
  for (int r=y1; r<y2; ++r) {
    for (int c=x1; c<x2; ++c) {
      temp[i++] = img->data[r * img->cols + c];
    }
    }*/

  img->cols = newr;
  img->rows = newc;

  free(img->data);
  img->data = temp;
  //free(orig->data);
  //free(orig);
  
  
  return 0;
}
int grayscale(Image *img){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    int intense = (int)(0.30*img->data[i].r + 0.59*img->data[i].g + 0.11*img->data[i].b);
    img->data[i].r = intense;
    img->data[i].g = intense;
    img->data[i].b = intense;
  }
  return 0;
}

int blur(Image *img, double sigma){
  if(sigma == 0){
    return 0;
  }
  
  /* get size of gaussian matrix */
  int rc = (int)(10*sigma);
  if(rc % 2 == 0){
    rc = rc+1;
  }
  
  double gaussian[rc][rc];
  double tempr = 0;
  double tempb = 0;
  double tempg = 0;
  double average = 0;

  /* calculate the weight of gaussian matrix */
  for(int x = 0; x<rc; x++){
    for(int y = 0; y < rc; y++){
      int dx = rc/2 - x; 
      int dy = rc/2 - y;
      gaussian[x][y] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
      average = average+gaussian[x][y];
    }
  }

  for(int i = 0; i < rc; ++i){
    for(int j = 0; j< rc; ++j){
      gaussian[i][j] /= average;
    }
  }

  for(int r = 0; r < img->rows; r++){
    for(int c = 0; c < img->cols; c++){
      //img->data[(r*(img->rows))+c]
      //int count = -(rc/2);
      //int m = 0;//rc
      //int n = 0;//rc
       for(int m = 0; m < rc; m++){
	for(int n = 0; n < rc; n++){
	  int i = (-rc/2)+m;
	  int j = (-rc/2)+n;
	  if((0 <=r+i) && (0 <= c+j) && (r+i < img->rows) && (c+j < img->cols) ){
	    tempr = tempr + (double)img->data[(r+i)*(img->rows) + c+j].r * gaussian[m][n];
	    tempg = tempg + (double)img->data[(r+i)*(img->rows) + c+j].g * gaussian[m][n];
	    tempb = tempb + (double)img->data[(r+i)*(img->rows) + c+j].b * gaussian[m][n];
	  }
	}
      }
      
      img->data[(r*(img->rows)) + c].r = (int)cap(tempr/average);
      img->data[(r*(img->rows)) + c].g = (int)cap(tempg/average);
      img->data[(r*(img->rows)) + c].b = (int)cap(tempb/average);

      tempr = 0;
      tempb = 0;
      tempg = 0;
    }
  }
  printf("done!\n");
  return 0;
}

int sharpen(Image* img, double sigma, double amt){
  /*Pixel* ptemp= malloc(sizeof(Pixel)*img->rows*img->cols);
  for(int j = 0; j < (img->rows)*(img->cols); j++){
    img->data[i] = ptemp[i];
    }*/

  Image temp = *img; //malloc(sizeof(Image));
  //temp->data = *ptemp;
  
  blur(&temp, sigma);

  for(int i = 0; i<(img->rows)*(img->cols); i++){
    img->data[i].r = img->data[i].r + img->data[i].r - temp.data[i].r;
    img->data[i].g = img->data[i].g + img->data[i].g - temp.data[i].g;
    img->data[i].b = img->data[i].b + img->data[i].b - temp.data[i].b;
  }

  contrast(img, amt);
  
  //free(ptemp);
  //free(&temp.data);
  return 0;
}

int contrast(Image *img, double scale){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    img->data[i].r = (int)(cast(img->data[i].r, scale));
    img->data[i].g = (int)(cast(img->data[i].g, scale));
    img->data[i].b = (int)(cast(img->data[i].b, scale));
  }
  return 0;
}


double cast(double rgb, double scale){
  rgb = (rgb - (255 / 2)) / 255;
  rgb *= scale;
  if(rgb > 0.5){
    rgb = 0.5;
  }
  if (rgb < -0.5){
    rgb = -0.5;
  }
  rgb = (rgb - (255 / 2)) * 255;
  cap(rgb);
  return rgb;
}



double cap(double a){
  if(a>255){
    a = 255.0;
  }
  if(a<0){
    a = 0.0;
  }
  return a;
}

double sq(double b){
  b = (b)*(b);
  return b;
}
