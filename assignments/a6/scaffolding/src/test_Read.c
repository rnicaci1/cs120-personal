#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>

typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Pixel;

typedef struct _image{
  Pixel* data;
  int rows;
  int cols;
  int colors;
} Image;

Image readImage(char inFile[]);
int writePPMImageFile(Image *im, char *filename); 
int inverse(Image* img);
/* swap the color channel of the image*/
int swap(Image *img);
/* crop the image to given size if the size is not out of bound*/
int crop(Image* img, int x1, int y1, int x2, int y2);


int main(){

  char out[] = "outFile.ppm"; 

  Image img = readImage("nikatest2.ppm");
  //Image *im = &img;

  crop(&img, 60, 25, 600, 565);


  int j = writePPMImageFile(&img, out);
  if (j > 0){
    printf("yay again! %d %d %d %d", img.rows, img.cols, img.colors, j);
  } else {
    printf("ya fucked it %d %d %d", img.rows, img.cols, img.colors);
  }
  
  free(img.data);
  //free(im);
  return 0;

}

Image readImage(char inFile[]) {
  Image img;
  FILE* fp = fopen(inFile, "rb");
  if(!fp){
    printf("Cannot find image file!");
    exit(1);
  }
  
  char buffer[2];
  for (int i = 0; i < 2; i++){
    buffer[i] = fgetc(fp);
  }
  if(buffer[0] == 'P' && buffer[1] == '6'){
    char c;
    
    while(!isspace(c = fgetc(fp)));
    c=fgetc(fp);
    while(c == '#'){
      while((c=fgetc(fp)) != '\n');
    }
   
    
    ungetc(c, fp);
    
    fscanf(fp, "%d %d %d", &(img.rows), &(img.cols), &(img.colors));

    
    // allocate space for pixels
    Pixel *pix = NULL;
    pix = malloc(sizeof(Pixel) * img.rows * img.cols);
    
    img.data = pix;
    fread(img.data, sizeof(Pixel), (img.rows)*(img.cols), fp);
  }  
  fclose(fp);

  return img;
}


/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename) {
  FILE *fp = fopen(filename, "wb");
  if (!fp) {
    fprintf(stderr, "Error:ppmIO - unable to open file \"%s\" for writing!\n", filename);
    return 0;
  }
  
  /* write tag */
  fprintf(fp, "P6\n");
  /* write dimensions */
  fprintf(fp, "%d %d\n%d   ", im->rows, im->cols, im->colors);

  int written = fwrite(im->data, sizeof(Pixel), (im->rows)*(im->cols), fp);
    
  if (written != (im->rows * im->cols)) {
    fprintf(stderr, "Error:ppmIO - failed to write data to file!\n");
    return 0;
  }
  fclose(fp);
  return written;
}

int inverse(Image* img){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    img->data[i].r = 255- img->data[i].r;
    img->data[i].g = 255- img->data[i].g;
    img->data[i].b = 255- img->data[i].b;
  }
  return 0;
}

int swap(Image *img){
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    int temp = img->data[i].r;
    img->data[i].r = img->data[i].g;
    img->data[i].g = img->data[i].b;
    img->data[i].b = temp;
  }
  return 0;
}

int crop(Image* img, int x1, int y1, int x2, int y2){
  if((x1<0) || (y1<0) || (x2>img->cols) ||(y2>img->rows)||(x1>x2)||(y1>y2)){
    printf("bad input: cropping out of bound!");
    return 1;
  }

  Image * temp = malloc(sizeof(Pixel)*(y2-y1+1)*(x2-x1+1));
  Image * orig = img;
  int i = 0;

  for (int r=0; r<(y2-y1+1); ++r) {
    for (int c=0; c<(x2-x1+1); ++c) {
      temp->data[i] = img->data[r*(y1-1)+ x1-1];
      i++;
    }
  }

  img = temp;


  free(orig->data);
  free(orig);


  return 0;
}
