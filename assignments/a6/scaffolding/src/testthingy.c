#include <stdio.h>

double cap(double a);

void main(){
  double i = 257.5;
  i = cap(i);
  printf("%d", (int)i);
}

double cap(double a){
  if(a>255){
    a = 255.0;
  }
  if(a<0){
    a = 0.0;
  }
  return a;
} 
