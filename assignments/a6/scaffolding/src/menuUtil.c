/*
 *menuUtil.c
 *Intermediate Programming
 *Name: Rebecca Nicacio & Coco Li
 *Date Last Modified: 10/17/16
 *
 *This is where the main menu is displayed and
 *the user can enter in their choices of image
 *as well as image manipulation functions
 */

#include <stdio.h>
#include <string.h>
#include "imageManip.h"
#include "menuUtil.h"
#include <ctype.h>
#include <stdlib.h>

int menu();

/*Displays the main menu and takes care of input*/
int menu(){

  //Prints the menu
  printf("Main menu:\n");
  printf("        r <filename> - read image from <filename>\n");
  printf("        w <filename> - write image to <filename>\n");
  printf("        c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n");
  printf("        i - invert intensities\n");
  printf("        s - swap color channels\n");
  printf("        g - convert to grayscale [not done yet]\n");
  printf("        bl <sigma> - Gaussian blur with the given radius (sigma) [not done yet]\n");
  printf("        sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma) [not done yet]\n");
  printf("        br <amt> - change brightness (up or down) by the given amount [not done yet]\n");
  printf("        cn <amt> - change contrast (up or down) by the given amount [not done yet]\n");
  printf("        q - quit\n");

  printf("Enter choice:");
  
  Image img;       //declare an Image struct
  int counter = 0;
  
  char *bufferc = malloc(sizeof(char)*150); //initialize a buffer to store/read the user input
  scanf("%s", bufferc);

  //Keep the user in the menu until they enter 'q' for quit
  while(strcmp("q", bufferc) != 0){

    //User enters 'r', call readImage
    if(strcmp("r", bufferc) == 0){

      scanf("%s", bufferc);
      img = readImage(bufferc);
      printf("Reading from %s\n", bufferc);
      counter = 1;

    } else if(strcmp("w", bufferc)==0){ //User enters 'w', write the image to the specified output file

      scanf("%s", bufferc);
      printf("Writing to %s\n", bufferc);
      writePPMImageFile(&img, bufferc);
      
    }else if(strcmp("i", bufferc)==0){ //User enters 'i', call inverse() function to invert the colors
      inverse(&img);                   //Takes in the pointer to the read Image
      printf("Inverting intensity...\n");
      
    } else if(strcmp("s", bufferc)==0){ //User enters 's', call swap() to swap the color values
      swap(&img);                       //Takes in a pointer to the read Image 
      printf("Swapping Color Channels...\n");
      
    } else if(strcmp("c", bufferc) ==0){ //User enters 'c', call crop() to crop along the specified dimensions
      int x1, x2, y1, y2;
      scanf("%d", &x1);                  //Read the user's input and store it into 4 int values
      scanf("%d", &y1);
      scanf("%d", &x2);
      scanf("%d", &y2);

      printf("Cropping from: (%d %d) to (%d %d)\n", x1, y1, x2, y2);
      crop(&img, x1, y1, x2, y2);        //Takes in the Image pointer and the dimensions 

      /*Phase 2 unimplemented functions */
    } else if (strcmp("g", bufferc) ==0){
      printf("grayscale not yet inplemented\n");
      
    } else if(strcmp("bl", bufferc) ==0){
      printf("blur not yet inplemented\n");
      
    } else if(strcmp("sh", bufferc) ==0){
      printf("sharpen not yet inplemented\n");
      
    } else if(strcmp("br", bufferc) ==0){
      printf("brightness change not yet inplemented\n");
      
    } else if(strcmp("cn", bufferc) ==0){
      printf("contrast change not yet implemented\n");
      
    } else if(strcmp("q",bufferc) == 0){
      printf("goodbye!");
      free(bufferc);
      return 0;
      
    }else { //If the user enters bad input
      printf("Invalid input, try again\n");
    }

    /*Displays menu again until the user quits*/
    printf("Main menu:\n");
    printf("        r <filename> - read image from <filename>\n");
    printf("        w <filename> - write image to <filename>\n");
    printf("        c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n");
    printf("        i - invert intensities\n");
    printf("        s - swap color channels\n");
    printf("        g - convert to grayscale\n");
    printf("        bl <sigma> - Gaussian blur with the given radius (sigma)\n");
    printf("        sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n");
    printf("        br <amt> - change brightness (up or down) by the given amount\n");
    printf("        cn <amt> - change contrast (up or down) by the given amount\n");
    printf("        q - quit\n");

    printf("Enter choice:");
    
    scanf("%s",bufferc);

  }

  /*Free the allocated memory*/
  if(counter != 0){
    free(img.data);
  }

  free(bufferc);
  return 0;

}
