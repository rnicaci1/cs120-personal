/*                                                                            
 *imageManip.c                           
 *Intermediate Programming                                                    
 *Name: Rebecca Nicacio & Coco Li                                
 *Date Last Modified: 10/17/16                                                
 *               
 *Handles all of the image manipulation functions:
 *swap, invert, crop.                                             
 */


#include <stdio.h>
#include "imageManip.h"
#include <stdlib.h>

/* inver color of the image*/
int inverse(Image* img);
/* swap the color channel of the image*/
int swap(Image *img);
/* crop the image to given size if the size is not out of bound*/
int crop(Image* img, int x1, int x2, int y1, int y2);

/*Takes in the read image and inverts the pixel color values*/
int inverse(Image* img){

  //invert RGB values in each pixel of the image
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    img->data[i].r = 255- img->data[i].r; //invert red
    img->data[i].g = 255- img->data[i].g; //invert green
    img->data[i].b = 255- img->data[i].b; //invert blue
  }
  
  return 0;
}

/*Takes in the read image and swaps the pixel color values
 *ie. red becomes blue, blue becomes green */
int swap(Image *img){

  //swap the RGB values for each pixel
  for(int i = 0; i < (img->rows) * (img->cols); i++){
    int temp = img->data[i].r;
    img->data[i].r = img->data[i].g; //swap red value
    img->data[i].g = img->data[i].b; //swap green value
    img->data[i].b = temp;           //swap blue value
  }
  
  return 0;
}

/*Takes in the read image as well as the cropping dimensions
 *and then crops the image*/
int crop(Image* img, int x1, int y1, int x2, int y2){

  //If the user inputs invalid cropping dimensions, return an error
  if((x1<0) || (y1<0) || (x2>img->cols) ||(y2>img->rows)||(x1>x2)||(y1>y2)){
    printf("Bad input: cropping out of bounds!");
    return 1;
  }
  
  int newr = y2-y1;//parameter of new number of rows
  int newc = x2-x1;//parameter of new number of columns
  printf("new cropping parameters: rows: %d, columns: %d\n", newr, newc);//tells the use the new parameters if they want to crop again

  Pixel * temp = malloc(sizeof(Pixel)*newr*newc);//allocate new pixel array to hold the cropped image pixels 
  int i = 0;
  for(int r = 0; r<img->rows; r++){//loop through the entire original array
    for(int c = 0; c<img->cols; c++){
      if((y1<= r) && (r<y2) && (x1<=c) && (c<x2)){
	/*if the pixel in the original pixel array is within the cropped parameter, 
	  add it to the new pixel array storing the cropped pixels*/
	temp[i++]= img->data[(r*(img->rows))+c];
      }
    }
   }

  img->cols = newr;//the original rows and columns are read in inverse, so to fix this pass
  img->rows = newc;//the new file with the parameters of rows and columns reversed as well

  free(img->data);//free the old array of pixels
  img->data = temp;//set the image pointint to now the cropped array of pixels 

  return 0;
}
