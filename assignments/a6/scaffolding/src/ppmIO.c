/*                                                          
 *ppmIO.c                                                          
 *Intermediate Programming                                                
 *Name: Rebecca Nicacio & Coco Li                                             
 *Date Last Modified: 10/17/16                                      
 *
 *This file contains functions that reads the ppm file into image structures 
 *that contain pixel arrays and also writes the Image structure into new ppm images 
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "ppmIO.h"
#include <ctype.h>

/* read file function */
Image readImage(char inFile[]);
/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename);


Image readImage(char inFile[]) {
  Image img;//create image struct to be used throughout the program
  FILE* fp = fopen(inFile, "rb");//opens the file according to user input name
  if(!fp){//if fails to open file, informs the user about invalid file name and quit program
    printf("Cannot find image file!");
    exit(1);
  }

  
  char buffer[2];
  for (int i = 0; i < 2; i++){
    buffer[i] = fgetc(fp);
  }
  /* test the first two character to be P6, hence test the file to be ppm image file
   * if is ppm file, test for comments in between "P6" and file demension */
  if(buffer[0] == 'P' && buffer[1] == '6'){
    char c;
    while(!isspace(c = fgetc(fp)));
    c=fgetc(fp);
    while(c == '#'){
      while((c=fgetc(fp)) != '\n');
    }
   
    
    ungetc(c, fp);

    /*scan for dimensions and colors*/
    fscanf(fp, "%d %d %d", &(img.rows), &(img.cols), &(img.colors));
    
    // allocate space for pixels
    Pixel *pix = NULL;
    pix = malloc(sizeof(Pixel) * img.cols * img.rows);

    //have image point to pixel array, read pixels in ppm file into pixel array
    img.data = pix;
    fread(img.data, sizeof(Pixel), (img.rows)*(img.cols), fp);
   
  }  
  fclose(fp);

  return img;
}


/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename) {
  FILE *fp = fopen(filename, "wb");//open file to write into
  if (!fp) {//if cannot open file, tell user about invalid input and quit program
    fprintf(stderr, "Error:ppmIO - unable to open file \"%s\" for writing!\n", filename);
    return 0;
  }
  
  /* write tag indicating its ppm file*/
  fprintf(fp, "P6\n");
  /* write dimensions */
  fprintf(fp, "%d %d\n%d   ", im->rows, im->cols, im->colors);

  /*write the image*/
  int written = fwrite(im->data, sizeof(Pixel), (im->rows)*(im->cols), fp);

  /*if written dimensions doesn't match input dimensions, return invalid result and quit program*/
  if (written != (im->rows * im->cols)) {
    fprintf(stderr, "Error:ppmIO - failed to write data to file!\n");
    return 0;
  }
  fclose(fp);
  return written;
}
