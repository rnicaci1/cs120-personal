#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
//#include "ppmIO.h"
//#include "menuUtil.h"

typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Pixel;

typedef struct _image{
  Pixel* data;
  int rows;
  int cols;
  int colors;
} Image;

int getLine(char* wordarr);

int main () {
  char* arr = malloc(sizeof(char)*50);
  //char *temp = arr;
  int j = 0;
  j = getLine(arr);
  for(int i = 0; i < j; i++){
    printf("%c", *(arr+i));
  }

  /*char *word = malloc(sizeof(char)*5);
  scanf("%s", word);

  for(int k = 0; k < 5; k++){
    printf("%c", *(word+k));
    }*/

  free(arr);
  //free(word);
  
  return 0;
}

int getLine(char* wordarr){
  char c;
  int i = 0;
  while((c=getchar())!= '\n'){
    *(wordarr + i) = c;
    i++;
  }
  return i;
}
