#ifndef _CS120_SCAFFOLD_IMAGEMANIP_H
#define _CS120_SCAFFOLD_IMAGEMANIP_H

#include <stdio.h>
#include "ppmIO.h"


/* invert the image colors */
int inverse(Image* img);
/* swap color channels */
int swap(Image *im);
/* crop the image */
int crop(Image *im, int x1, int y1, int x2, int y2);

#endif
