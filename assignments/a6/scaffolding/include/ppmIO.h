#ifndef _CS120_SCAFFOLD_PPMIO_H
#define _CS120_SCAFFOLD_PPMIO_H

#include <stdio.h>

typedef struct _pixel {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} Pixel;

typedef struct _image{
  Pixel* data;
  int rows;
  int cols;
  int colors;
}Image;

/* read file function */
Image readImage(char inFile[]);
/* wrapper that takes a filename (handles open/closing the file) */
int writePPMImageFile(Image *im, char *filename);

#endif
