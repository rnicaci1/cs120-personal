#include <iostream>
#include <vector>
#include <fstream>

class List{
 public:
  void eraseLast(){list.pop_back();} 
  std::vector<std::string> returnList(){return list;}
  int returnNumFiles(){return numFiles;}
  int size(){return list.size();}
  
  void readFiles(std::string listFile){
    std::ifstream fin;
    fin.open(listFile);
    if(!fin.is_open()){
      std::cerr << "Unable to open '" << listFile << "'\n";
      exit(1);
    }
    
    std::string name;
    while(fin >> name){
      list.push_back(name);
      numFiles++;
    }
    fin.close();

    
    //for(auto iter = list.begin();
    //	iter != list.end();
    //	iter++){
    //  std::cout << *iter << "\n";
    // }
    //std::cout << numFiles << "\n"; 
  }
    
 private:
  std::vector<std::string> list;
  int numFiles = 0; 
};
