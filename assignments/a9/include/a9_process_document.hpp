#ifndef _A9_PROCESS_DOCUMENT_HPP
#define _A9_PROCESS_DOCUMENT_HPP
#include <vector>
#include <map>
#include <string>
#include <utility>

class Document {
public:
  Document(){}

  void readFileList(std::vector<std::string> listFile, int index);//std::string fileName);
  void addText(std::string fileName);
  //std::vector<std::string> readNames(std::string listFile);

  //std::vector<std::string> listFile;
  std::map< std::string, int> returntxt(){ return text;}
  int returnNumWords(){return numWords;}


  std::map<std::string, std::pair<double, double> > weighted;
  std::map< std::string, int> text;
  void clear(); 
  //int numWords = 0;
  
private:
  int numWords; 
  //int numWords = 0;
};

#endif  
