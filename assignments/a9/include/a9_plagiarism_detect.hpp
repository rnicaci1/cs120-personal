#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include "a9_process_document.hpp"

class Compare{
public:
  Compare() {;}
  void calcRawFrequency(Document *doc);//, int N, int numWords);
  //void calcOtherRawFrequency();
  double compare();//int N);
  double calcSim(Document *doc1, Document *doc2);
  void calcIDF();
  
  Document source;
  Document other; 
  
private:
  //std::map<std::string, std::pair<double, double> > weightedother;
  //std::map<std::string, std::pair<double, double> > weightedself;
};
