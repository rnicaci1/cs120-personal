#include "../include/catch.hpp"
#include "../include/a9_process_document.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>

using std::string;
using std::vector;
using std::map; 

TEST_CASE("addText", "[addText]"){
  Document doc1;
  doc1.addText("../src/test1.txt");
  string result1;
  map<string, int> text1 = doc1.returntxt();

  for(auto iter = text1.begin();
      iter != text1.end();
      iter++){
    result1 = result1 + iter->first + " ";
  }

  
  Document doc2;
  doc2.addText("../src/non-existent.txt");
  string result2;
  map<string, int> text2 = doc2.returntxt();

  for(auto iter = text2.begin();
      iter != text2.end();
      iter++){
    result2 = result2 + iter->first + " ";
  }
  

  REQUIRE(result1 == "Some are comedy. differences family. human in note obvious of on serious, some the thrive us ");
  REQUIRE(result2 == "");
}
