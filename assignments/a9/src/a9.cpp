#include <iostream>
#include <string>
#include <map>
#include "../include/a9_plagiarism_detect.hpp"
#include "../include/a9_list_control.hpp"

using std::string;
using std::map;

int main(int argc, char* argv[]){
  if(argc < 2 || argc > 3){
    std::cerr << "Incorrct number of command line arguments supplied. \n";
    return 1;
  }
  
  string files = argv[1];
  string sensitivity;
  double senseLevel = 0; 

  if(argc == 3){
    sensitivity = argv[2];
  }else{
    sensitivity = "m"; 
  }
  
  if(sensitivity.compare("l") == 0){
    senseLevel = 0.6;
  } else if(sensitivity.compare("m") == 0) {
    senseLevel = 0.75; 
  } else if(sensitivity.compare("h") == 0) {
    senseLevel = 0.85;
  } else {
    std::cerr << "Incorrect sensitivity command, try again!";
    return 1; 
  }

  if(senseLevel == 0){
    std::cerr << "Issue setting sensitivity level!";
    return 1; 
  }

  List listFile;
  listFile.readFiles(files);

  if(listFile.size() == 0){
    throw std::underflow_error("No file being compared!");
  }
  
  
  int times = listFile.size() - 1;
  while(times >= 1){
    Compare comp;
    //std::cout << "times = " << times << "\n"; 
    comp.source.readFileList(listFile.returnList(), times);


    for(int j = times - 1; 0 <= j; j--){
      //std::cout << "j =" <<j << "\n";
      comp.other.readFileList(listFile.returnList(), j);
      double sim = comp.compare();//listFile.returnNumFiles());

      if(sim > senseLevel){
	std::cout << (listFile.returnList())[times] << " " << (listFile.returnList())[j] << "\n";
      }

      comp.other.clear();
      comp.source.weighted.clear();
    }
    
    listFile.eraseLast();
    times = listFile.size() - 1;
    //comp.source.clear();
  }

  
  return 0;
  
}
