#include <iostream>
#include "../include/a9_plagiarism_detect.hpp"
using std::map;
using std::string;
using std::pair; 

double Compare :: compare(){//int N){
  double sim = 0; 
  if(source.text.size() != 0 && other.text.size()!= 0){
    calcRawFrequency(&source);//, N//, source.returnNumWords());
    /*for(auto iter = source.weighted.begin();
	iter != source.weighted.end();
	iter++){
      // std::cout << (iter->second).first << " " << (iter->second).second << "\n";
      //std::cout << iter->first << "\n";
    }*/
    calcRawFrequency(&other);
    /*for(auto iter2 = other.weighted.begin();
	iter2 != other.weighted.end();
	iter2++){
      std::cout << (iter2->second).first << " " << (iter2->second).second << "\n";
      }*/
    //calcOtherFrequency();
    calcIDF();
    
    sim = calcSim(&source, &other);
    
  } else {
    std::cerr << "empty documents!";
    exit(-1);
  }
  return sim; 
}

/*overload function, adjust raw frequency for the source document*/
void Compare :: calcRawFrequency(Document *doc){//, int N, int numWords){
  //map<string, int> text = doc.returntxt();
  double maxfreq = 0.0; 
  /*find the biggest raw frequency of the document*/
  for(auto iter = doc->text.begin();
      iter != doc->text.end();
      iter++){
    if(iter->second > maxfreq){
      maxfreq = iter->second; 
    }
  }
  //std::cout << maxfreq << "\n";

  //double IDF = std::log(N/(double)((*doc).returnNumWords()));

  /*adjust this for the new frequency word map*/
  for(auto iter = doc->text.begin();
      iter != doc->text.end();
      ++iter){
     double TF = 0.5 + 0.5* ((iter->second)/maxfreq);

     pair<double, double> weightedAverage(TF, 0.5);
     doc->weighted[iter->first] = weightedAverage;
  }  
  
}

void Compare :: calcIDF(){
  for(auto iter = this->source.weighted.begin();
      iter != this->source.weighted.end();
      ++iter){
    if(other.weighted.count(iter->first)){
      iter->second.second = ((double)2)/2;
      other.weighted.at(iter->first).second = ((double)2)/2;
    }
  }
}

double Compare :: calcSim(Document *doc, Document *doc2){ 
  double thing = 0; 
  double sim = 0;
  double wij2 = 0;
  double wiq2 = 0;
  
  //auto iter2 = (*doc2).weighted.begin();
  for(auto iter = (doc->weighted).begin();
      iter != (doc->weighted).end();
      ++iter){
    
    /* double wij2 = 0;
    double wiq2 = 0;*/
    wij2 += std::pow((iter->second).first * (iter->second).second, 2.0);
    //std::cout << wij2 << "\n";

    string word = iter->first; 

    //if(iter2 != (*doc2).weighted.end()){
    if((*doc2).weighted.count(word)){
      //std::cout << word << "\n";
      double w2TF = doc2->weighted.at(word).first;
      double w2IDF = doc2->weighted.at(word).second;
      //std::cout<< "w2IDF = " << w2IDF << " "; 

      wiq2 += std::pow(w2TF * w2IDF, 2.0);
      thing += (((iter->second).first * (iter->second).second) * (w2TF * w2IDF));

      //std::cout << wiq2 << "\n";
      //std::cout << sim << "\n";
    }
    
  //++iter2;
  }
  //std::cout << wij2 << "\n";
  if(thing != 0){
    sim = thing/(std::sqrt(wij2) * std::sqrt(wiq2));
    double dist = std::acos(sim)/3.14159265358979323846;
    sim = (1 - dist);
    std::cout << sim << "\n";
    return sim;
  }else {
    return 0; 
  }
  //sim = std::abs(std::cos(sim));
}
