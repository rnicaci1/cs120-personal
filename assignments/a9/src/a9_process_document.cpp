#include <iostream>
#include <fstream>
#include <map>
#include "../include/a9_process_document.hpp"

using std::map;
using std::string;
using std::vector; 

void Document :: readFileList(vector<string> listFile, int index){//std::string fileName){
  //listFile = readNames(fileName);//store local copy, when recurring then delete individual copy each time 
  addText(listFile[index]);
}

/*
std::vector<std::string> Document :: readNames(std::string listFile){
  std::vector<std::string> names;
  std::ifstream fin;
  fin.open(listFile);
  if(!fin.is_open()){
    std::cerr << "Unable to open '" << listFile << "'\n";
    exit(-1);
  }
  std::string name;
  while(fin >> name){
    names.push_back(name);
  }
  fin.close();
  
  return names;
}*/

void Document :: addText(std::string fileName){
  std::ifstream fin;
  this->numWords = 0; 
  fin.open(fileName);
  if(!fin.is_open()){
    std::cerr << "Unable to open '" << fileName << "', file will be skipped\n";
    return;
  }
  
  string key;
  while(fin >> key){
    if(text.count(key)){
      ++text.at(key);
    } else{
      text[key] = 1;
      ++(this->numWords);// = (this->numWords) + 1;
      //std::cout << (this->numWords) << "\n"; 
    }
  }
  
  fin.close();
}

void Document :: clear(){
  this->numWords = 0;
  text.clear();
  weighted.clear();
}
